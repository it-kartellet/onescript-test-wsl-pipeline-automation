start webserver:

# php -S localhost:8000
sudo /etc/init.d/nginx stop
sudo /etc/init.d/nginx start


PHP on Windows PC
https://windows.php.net/download#php-8.0


Composer 
https://getcomposer.org/Composer-Setup.exe
https://www.frobiovox.com/posts/2016/08/16/basic-hello-world-with-composer-and-php.html



Run Linux Nginx On Windows with WSL
https://www.ntweekly.com/2017/09/08/run-linux-nginx-windows-server-insider-wsl/


Remote development in WSL
https://code.visualstudio.com/docs/remote/wsl-tutorial


PHP debugging
https://www.itwriting.com/blog/10213-setting-up-php-for-development-on-windows-services-for-linux-in-windows-10.html


Next, test to make sure that there are no syntax errors in any of your Nginx files:
sudo nginx -t


Saturday
sudo apt install php
sudo apt-get install php-fpm

sudo systemctl enable nginx
sudo service status nginx
sudo systemctl start nginx

Start from windows:
wsl sudo service nginx start
sudo service php7.4-fpm start
wsl sudo service nginx start




sudo apt install php php-cli php-fpm php-json php-common php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath
sudo apt install php-cgi


Fix save problems from VSCode
sudo chown -R $USER:$USER /var/www/html

ps -aux

service --status-all


sudo service nginx restart
sudo service appache2 stop


#important to start
sudo service php7.4-fpm restart


Important regarding debugging
https://xdebug.org/docs/install#linux

sudo apt-get install php-xdebug

cd /home/lakn
wget http://xdebug.org/files/xdebug-3.0.2.tgz

Install the pre-requisites for compiling PHP extensions. These packages are often called 'php-dev', or 'php-devel', 'automake' and 'autoconf'.

sudo apt-get install php-dev

Unpack the downloaded file with 
tar -xvzf xdebug-3.0.2.tgz
cd xdebug-3.0.2
now pwd = /home/lakn/xdebug-3.0.2


Run: phpize (See the FAQ if you don't have phpize).

As part of its output it should show:

Configuring for:
...
Zend Module Api No:      20190902
Zend Extension Api No:   320190902
If it does not, you are using the wrong phpize. Please follow this FAQ entry and skip the next step.

Run: ./configure --enable-xdebug
Run: make
Run: make install



un: cp modules/xdebug.so /usr/lib/php/20190902
Update /etc/php/7.4/cli/php.ini (code /etc/php/7.4/cli/php.ini)and change the line
zend_extension = /usr/lib/php/20190902/xdebug.so
Make sure that zend_extension = /usr/lib/php/20190902/xdebug.so is below the line for OPcache.
Please also update php.ini files in adjacent directories, as your system seems to be configured with a separate php.ini file for the web server and command line.


sudo service nginx restart