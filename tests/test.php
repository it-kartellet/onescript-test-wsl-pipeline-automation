<?php

// Autoload files using Composer autoload
require_once __DIR__ . '/../vendor/autoload.php';

use Acme\Page;
use Acme\Greetings;

$page1 = new Page();

echo Greetings::sayHelloWorld();